<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ValiDemoController extends Controller
{
    public function index()  // indexページの表示 //
  {
      return view('validation.index');
  }

  private function add($num1, $num2) {
    $result = $num1 + $num1;
    return $result;
}

public function create(Request $request)
{
    // バリデーションのルール //
    $validateRules = [
        'num1'=>'required|numeric|integer',
        'num2'=>'required|numeric|integer',
    ];

    // エラーメッセージ //
    $validateMessages = [
        "required"=> "必須項目です。",
        "numeric" => "数値で入力してください。",
        "integer" => "整数で入力してください。",
    ];

    // バリデーションをインスタンス化 //
    $this -> validate($request, $validateRules, $validateMessages);


    // 投稿内容の受け取って変数に入れる
    $num1 = $request->input('num1');
    $num2 = $request->input('num2');

    $result = $this->add($num1, $num2);

    return view('validation.index')->with([
         "num1" => $num1,
         "num2"  => $num2,
         "result" => $result,
    ]);
   }

}
